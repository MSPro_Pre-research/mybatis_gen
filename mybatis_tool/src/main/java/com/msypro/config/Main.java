package com.msypro.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
/*-------------------------------生成配置路径----------------------------------------------------------*/	
  //前端首页生成地址
  private static final String DISK_HTML = "D:/test/html/";
  
  //后端代码生成
  private static final String DISK_JAVA = "E:\\工作内容\\resources\\";
  
/*-------------------------------生成配置路径----------------------------------------------------------*/	
  
/*-------------------------------用户说明信息----------------------------------------------------------*/	
  //作者信息
  private static final String AUTHOR = "XiaoMa Pro";
  
/*-------------------------------用户说明信息----------------------------------------------------------*/
  
  
/*-------------------------------数据库配置信息---------------------------------------------------------*/  
  //数据库驱动
  private static final String COM_MYSQL_JDBC_DRIVER = "com.mysql.jdbc.Driver";
  
  //数据配置信息
//  private static final String URL = "jdbc:mysql://127.0.0.1:3306/obe_license?characterEncoding=utf8&useSSL=false";
//  private static final String URL = "jdbc:mysql://122.14.210.101:3306/strom?characterEncoding=utf8&useSSL=false";
  private static final String URL = "jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf8&useSSL=false";
  
  //数据库用户名
  private static final String USERNAME = "root";
  
  //数据库密码
  private static final String PASSWORD = "root";
  
  //表名
  private static final String[] TABLENAMES = {"repo_table"};
/*-------------------------------数据库配置信息---------------------------------------------------------*/  
  
/*-------------------------------工程生成配置信息---------------------------------------------------------*/   
  //项目名称
  private static final String PROJECTNAME = "user";
  //包名
//  private static final String PACKAGENAME = "com.gx.license.license.obe";
   private static final String PACKAGENAME = "com.gx.obe.server.management.evaluation";

  // controllers 包名
  private static final String CONTROLLER = "controller";
  // services 包名
  private static final String SERVICE = "service"; 
  // entity 包名
  private static final String ENTITY = "entity"; 
  // mapper 包名
  private static final String MAPPER = "dao";
  // XML 包名
  private static final String XML = "mapper"; 
  
  //设置生成的Controller接口文件名， %s 会自动填充表实体属性！
  private static final String CONTROLLER_NAME = "%sController";
 
  //设置生成的service文件名的名字的首字母是否为I
  private static final String SERVICE_NAME = "%sService";
  
  //设置生成的ServiceImp文件名的名字的首字母是否为Imp
  private static final String SERVICEIMP_NAME = "%sServiceImpl";
  
  //设置生成的Mapper接口的名字的首字母是否为I
  private static final String MAPPER_NAME = "%sMapper";
  
  //设置生成的Xml接口的名字的首字母是否为I
  private static final String Xml_NAME = "%sMapper";
  
/*-------------------------------工程生成配置信息---------------------------------------------------------*/ 
 
/*-------------------------------是否生成前端页面---------------------------------------------------------*/ 
  private static final Boolean ISHTML = true;
  
/*-------------------------------是否生成前端页面---------------------------------------------------------*/ 
  
 
  /**
   * 代码生成（包含配置信息说明）
 * @param args
 */
public static void main(String[] args) {
	
/*-------------------------------1. 全局配置---------------------------------------------------------*/ 
    // 选择 freemarker 引擎，默认 Veloctiy
    // mpg.setTemplateEngine(new FreemarkerTemplateEngine());
    // 全局配置
    final GlobalConfig gc = new GlobalConfig();
    gc.setOutputDir(DISK_JAVA + "/" + PROJECTNAME + "/src/main/java");
    gc.setFileOverride(true);
    gc.setActiveRecord(true);					// 不需要ActiveRecord特性的请改为false
    gc.setEnableCache(false);					// XML 二级缓存
    gc.setBaseResultMap(true);					// XML ResultMap
    gc.setBaseColumnList(true);					// XML columList
    //gc.setKotlin(true);						//是否生成 kotlin 代码
    gc.setFileOverride(true);  					// 文件覆盖
    gc.setAuthor(AUTHOR);						//作者信息
    gc.setIdType(IdType.UUID); 					// 主键策略
    gc.setBaseResultMap(true);					//生成基本的resultMap
    gc.setBaseColumnList(true);					//生成基本的SQL片段
    gc.setControllerName(CONTROLLER_NAME); 		//Controller名称
    gc.setServiceName(SERVICE_NAME);			//Service名称
    gc.setMapperName(MAPPER_NAME);				//Mapper名称
    gc.setXmlName(Xml_NAME);					//Xml名称
    gc.setServiceImplName(SERVICEIMP_NAME);		//ServiceImp名称
/*-------------------------------全局配置---------------------------------------------------------*/ 
    
/*-------------------------------2.数据源配置---------------------------------------------------------*/     
    DataSourceConfig dsc = new DataSourceConfig();
    dsc.setDbType(DbType.MYSQL);
    dsc.setTypeConvert(new MySqlTypeConvert() {
      // 自定义数据库表字段类型转换---可选
      public IColumnType processTypeConvert(String fieldType) {
        System.out.println("转换类型：" + fieldType);
        //processTypeConvert 存在默认类型转换，如果不是你要的效果请自定义返回、非如下直接返回。
        return super.processTypeConvert(gc, fieldType);
      }
    });
    dsc.setDriverName(COM_MYSQL_JDBC_DRIVER);
    dsc.setUrl(URL);
    dsc.setUsername(USERNAME);
    dsc.setPassword(PASSWORD);

/*-------------------------------数据源配置---------------------------------------------------------*/   
  
/*-------------------------------3.策略配置---------------------------------------------------------*/     
    StrategyConfig strategy = new StrategyConfig();
    //strategy.setCapitalMode(true);// 全局大写命名 ORACLE 注意
    //strategy.setTablePrefix(new String[] { "tlog_", "tsys_" });// 此处可以修改为您的表前缀
    //strategy.setDbColumnUnderline(true)  // 指定表名 字段名是否使用下划线
    strategy.setNaming(NamingStrategy.underline_to_camel);// 数据库表映射到实体的命名策略
    strategy.setInclude(TABLENAMES); // 需要生成的表
    
    // 排除生成的表
    // strategy.setExclude(new String[]{"test"}); 
    
    // 自定义实体父类
    // strategy.setSuperEntityClass("com.baomidou.demo.TestEntity");
    
    // 自定义实体，公共字段
     strategy.setSuperEntityColumns(new String[] { "repo_id", "age" });
    
    // 自定义 mapper 父类
    // strategy.setSuperMapperClass("com.baomidou.demo.TestMapper");
    
    // 自定义 service 父类
    // strategy.setSuperServiceClass("com.baomidou.demo.TestService");
    
    // 自定义 service 实现类父类
    // strategy.setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl");
    
    // 自定义 controller 父类
    // strategy.setSuperControllerClass("com.baomidou.demo.TestController");
    
    // 实体是否生成字段常量（默认 false）|public static final String ID = "test_id";
    strategy.setEntityColumnConstant(true);
    
    // 实体是否为构建者模型（默认 false）|public User setName(String name) {this.name = name; return this;}
    //strategy.setEntityBuilderModel(true);
/*-------------------------------策略配置---------------------------------------------------------*/  

/*-------------------------------4.工程配置---------------------------------------------------------*/  
    // 包配置
    PackageConfig pc = new PackageConfig();
    pc.setParent(PACKAGENAME);
    pc.setController(CONTROLLER);// controller
    pc.setService(SERVICE);//servcie
    pc.setEntity(ENTITY);//entity
    pc.setMapper(MAPPER);//dao
    pc.setXml(XML);//mapper.xml
/*-------------------------------工程配置---------------------------------------------------------*/  
    
    
/*-------------------------------5.自定义配置---------------------------------------------------------*/    

    // 注入自定义配置，可以在 VM 中使用 cfg.test
    InjectionConfig config = new InjectionConfig() {
      @Override
      public void initMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("test", this.getConfig().getGlobalConfig().getAuthor()+"代码生成完毕");
        this.setMap(map);
      }
    };
/*-------------------------------自定义配置---------------------------------------------------------*/ 

    
/*-------------------------------6.前端生成配置---------------------------------------------------------*/
//    List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
//    focList.add(new FileOutConfig("/templates/list.html.vm") {
//      @Override
//      public String outputFile(TableInfo tableInfo) {
//        // 自定义输入文件名称
//        return DISK_HTML + tableInfo.getEntityName() + "ListIndex.html";
//      }
//    });
//    cfg.setFileOutConfigList(focList);
//
//    focList.add(new FileOutConfig("/templates/add.html.vm") {
//      @Override
//      public String outputFile(TableInfo tableInfo) {
//        return DISK_HTML + tableInfo.getEntityName() + "Add.html";
//      }
//    });
//    cfg.setFileOutConfigList(focList);
//
//    focList.add(new FileOutConfig("/templates/update.html.vm") {
//      @Override
//      public String outputFile(TableInfo tableInfo) {
//        return DISK_HTML + tableInfo.getEntityName() + "Update.html";
//      }
//    });
//    cfg.setFileOutConfigList(focList);
/*-------------------------------前端生成配置----------------------------------------------------*/  

/*-------------------------------7.后端生成配置--------------------------------------------------*/  
    /**
     * 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
     * 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
     * 模块如果设置 空 OR Null 将不生成该模块。
     */
    TemplateConfig tc = new TemplateConfig();
    tc.setController("/templates/controller.java.vm");
    tc.setService("/templates/service.java.vm");
    tc.setServiceImpl("/templates/serviceImpl.java.vm");
    tc.setEntity("/templates/entity.java.vm");
    tc.setMapper("/templates/mapper.java.vm");
    tc.setXml("/templates/mapper.xml.vm");
/*-------------------------------后端生成配置----------------------------------------------------*/     

    
/*-------------------------------8.全局配置------------------------------------------------------*/        
	final AutoGenerator mpg = new AutoGenerator();
    mpg.setGlobalConfig(gc);
    mpg.setDataSource(dsc);
    mpg.setStrategy(strategy);
    mpg.setPackageInfo(pc);
    if(ISHTML) {
        mpg.setCfg(config);	
    }
    mpg.setTemplate(tc);
/*-------------------------------全局配置---------------------------------------------------------*/          
    
/*-------------------------------9.执行操作-------------------------------------------------------*/
    mpg.execute();
/*-------------------------------执行操作---------------------------------------------------------*/    
    
/*-------------------------------测试获取注解-----------------------------------------------------*/    
    System.err.println(mpg.getCfg().getMap().get("test"));
/*-------------------------------测试获取注解-----------------------------------------------------*/ 
    
    
  }


}
